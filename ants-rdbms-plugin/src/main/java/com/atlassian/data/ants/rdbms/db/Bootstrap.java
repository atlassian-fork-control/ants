package com.atlassian.data.ants.rdbms.db;

import com.google.common.collect.ImmutableMap;
import org.flywaydb.core.Flyway;

import java.sql.Connection;
import javax.annotation.Nonnull;

import static com.atlassian.data.ants.rdbms.db.Names.*;
import static com.google.common.base.Preconditions.checkNotNull;

public class Bootstrap
{
    public static void execute(
            @Nonnull final Connection connection,
            @Nonnull final String id)
    {
        checkNotNull(connection);
        checkNotNull(id);

        // TODO: extract from connection
        final String flavour = "H2";

        final Flyway flyway = new Flyway();

        // use this bundle's classloader for the migration scripts, not Thread.currentThread().getContextClassLoader()
        flyway.setClassLoader(Bootstrap.class.getClassLoader());

        // different flyway schema table per plugin
        flyway.setTable(TABLE_PREFIX + "_" + id + "__FLYWAY");

        // use the provided connection
        flyway.setDataSource(new FlywayDataSource(new FlywayConnectionDelegate(connection)));

        // create the flyway bits even if there is existing database content
        flyway.setBaselineOnMigrate(true);

        // empty schema version is V0 (defaults to V1), to allow upgrade to V1
        flyway.setBaselineVersion("0");

        // use the database specific creation script
        flyway.setSqlMigrationPrefix(flavour + "_V");

        // use id for the table names and constants for others
        flyway.setPlaceholders(new ImmutableMap.Builder<String, String>()
                .put("ID", id)
                .put("TABLE_PREFIX", TABLE_PREFIX)
                .put("TABLE_SUFFIX_KEY", TABLE_SUFFIX_KEY)
                .put("TABLE_SUFFIX_VALUE", TABLE_SUFFIX_VALUE)
                .put("TABLE_SUFFIX_VERSIONS", TABLE_SUFFIX_VERSIONS)
                .put("COL_PREFIX", COL_PREFIX)
                .put("COL_KEY", COL_KEY)
                .put("COL_SHA1", COL_SHA1)
                .put("COL_VERSION", COL_VERSION)
                .put("COL_DATE", COL_DATE)
                .put("COL_VALUE", COL_VALUE)
                .build());

        // do the table creation / upgradation
        flyway.migrate();
    }
}
