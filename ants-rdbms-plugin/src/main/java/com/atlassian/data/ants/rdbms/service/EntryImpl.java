package com.atlassian.data.ants.rdbms.service;

import com.atlassian.data.ants.api.Entry;

import java.util.Date;

public class EntryImpl<T> implements Entry<T>
{
    private final String key;
    private final T value;
    private final int version;
    private final Date date;

    public EntryImpl(final String key, final T value, final int version, final Date date)
    {
        this.key = key;
        this.value = value;
        this.version = version;
        this.date = date;
    }

    @Override
    public String key()
    {
        return key;
    }

    @Override
    public T value()
    {
        return value;
    }

    @Override
    public int version()
    {
        return version;
    }

    @Override
    public Date date()
    {
        return date;
    }
}
